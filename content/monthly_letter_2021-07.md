Title: Monthly Report June 2021 - Documentation is key!
Date: 2021-07-19
Category: Monthly Letter
Authors: Codeberg e.V.

*(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V. this month; as usual published here with due delay. Not member of the non-profit association yet? [Consider joining it!](https://join.codeberg.org))*


Dear Codeberg e.V. Members and Supporters, once again it is time for some updates!

We are hosting 13510 repositories, created and maintained by 12127 users. Compared to one month ago, this is an organic growth rate of +1010 repositories (+8.1% month-over-month) and +759 users (+6.7%).

**Our new server:** As mentioned in the last newsletter, we installed the server into the data center at IN Berlin, as we discussed in the annual assembly earlier this year. This means, we have to pay for the power and space usage from now on, so we are working hard to get the systems up and running as soon as possible, and we are doing good [progress towards Codesearch](https://codeberg.org/Codeberg/Community/issues/379#issuecomment-241446) for a first burn test.

**Documentation:** In June, we have seen an increased amount of contributions to our Documentation, and we are really grateful for this. We are still aiming to turn the Codeberg Docs into a more complete end-user manual in a way that users from other Gitea instances can also profit from our effort. We also have our own Documentation Matrix Channel now - you can join it and ask your questions at [#codeberg-documentation:matrix.org](https://matrix.to/#/#codeberg-documentation:matrix.org) - and read about [our roadmap here](https://codeberg.org/Codeberg/Documentation/issues/134).
Also, there was the Codeberg "Docuthon" from 10 to 17 July: Since good code is more useful if it's properly documented, we asked everyone to add some lines to their project during this week, especially those project, who don't even carry a descriptive README. If you want to show your continued effort to document your project, feel free to add the "Docuthon" flag to your repo topics any time. Also, the Codeberg Docs are always looking for contribution, too.

**Community Issues:** Many people involved at Codeberg have been busy with the new server recently. We're working hard to deliver new features as soon as possible. On the other hand, we have had less time to take care of other parts of the platform. If you want to help, please consider providing a helping hand with our community issues: Confirming, Finding Details, Reporting Upstream or sending a fix. We have put some small instructions [on our Mastodon Channel](https://mastodon.technology/@codeberg/106445270542603984) - Thank you very much!


The machines are running at about 35% capacity, as usual we will scale up once again we approach the 66% threshold.

Codeberg e.V. has 134 members in total, these are 99 members with active voting rights and 34 supporting members, and one honorary member.

*Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this account group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username. If you need access to Codeberg e.V. report documents without being listed in the account group, please send an email.*

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929. 
