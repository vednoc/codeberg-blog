Title: Monthly Report December 2019
Date: 2020-01-08
Category: Monthly Letter
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month; as usual published here with due delay.)


Dear Codeberg e.V. Members and Supporters!

Happy New Year, and let's celebrate the 1-year Codeberg.org anniversary! A new month, time for updates.

For our annual member assembly on January 19th in Berlin, the nice guys from Topio invited us to use their project room in the old market hall in Berlin Moabit.

No worries if you cannot make it. Every active member will be able to exercise all rights remotely and online, even asynchronuously to avoid discrimination due to time zone discrepancies. Proposals for vote can be submitted via email or live, and all votes will be cast via voting tokens sent out to all active members in the week after the meeting. The advantage of physical attendance? Having a nice meet-up face to face, learn to know each other in person. Watch your inbox for the formal invite.

Slides and video of Andreas' Codeberg talk @sfscon are now online, check them out at https://www.sfscon.it/talks/codeberg-a-free-home-for-free-projects/.

Codeberg e.V. has 51 active members. We are hosting 1977 repositories, created and maintained by 4571 users. Compared to one month ago, this is an organic growth of rate of +200 repositories (+11% month-over-month), and +370 users (+9%). Note that the explosive increase of abusive user accounts has calmed down, we are planning to clean the database from non-activated accounts very soon (absolute user number will obviously be reset then in the statistics).

The machines are runnnig at about 48% capacity, we will scale up as soon we approach the 66% threshold. Backups are still managed by private offline machines by founding members at no cost; as soon Codeberg e.V. can afford it's own infrastructure, we are planning for an economic mid-end server with extendable redundant RAID disks, hardware donations may or may not allow us to afford those earlier. For the live server, our models suggested that rented cloud VMs are the most economic option for up to ~1000 users/repos, from then on it would potentially make sense to switch to rented dedicated servers. As we have now passed this threshold, we can now re-run initial calculations and check options for a logical next step.

Yours truly,
Codeberg e.V.

